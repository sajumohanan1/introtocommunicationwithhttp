﻿using Asp.NETWebAPI.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Asp.NETWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        //to test with some data, using the list
        //add some dummy data for this student list
        List<Student> student = new List<Student>()
        {
            new Student(){Id=1, Name="Saju"}, //1st record
            new Student(){Id=2, Name="Mohanan"}      //2nd record      
        };

        //perform HTTP verbs (Get, Post, Delete)

        [HttpGet] //get all records
        public IActionResult Gets()
        {
            if (student.Count == 0) //if the student count is zero
            {
                return NotFound("No list found");//perform if zero
            }
            return Ok(student);//perform this if there are some records
        }

        [HttpGet("GetStudent")] //get  one record -- GetStudent is the HttpGet Api controller name
        public IActionResult Get(int id)
        {
            var stud=student.SingleOrDefault(x=>x.Id==id); //property name must match with the id you are providing from the client side
            if (stud==null) 

            {
                return NotFound("record not found");

            }
            return Ok(stud);

        }

        [HttpPost] //create a new record
        public IActionResult Save(Student stud)
        {
            student.Add(stud);
            if (student.Count == 0)
            {
                return NotFound("no list found");
            }
            return Ok(student);
        }

        [HttpDelete]
        public IActionResult DeleteStudent(int id)
        {
            var stud = student.SingleOrDefault(x => x.Id == id); //property name must match with the id you are providing from the client side
            if (stud == null)

            {
                return NotFound("record not found");

            }
            student.Remove(stud);//remove the selected record
            if (student.Count == 0)
            {
                return NotFound("record not found");
            }
            return Ok(stud);
        }
       
    }
}
